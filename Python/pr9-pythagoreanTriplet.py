# -*- coding: utf-8 -*-
##Euler problem 9
##https://projecteuler.net/problem=9

## A Pythagorean triplet is a set of three natural
## numbers, a < b < c, for which,
## a^2 + b^2 = c^2
## For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

## There exists exactly one Pythagorean triplet
## for which a + b + c = 1000.
## Find the product abc.

## Ricardo Cedeño Montaña MSc.
## Berlin, Germany. 07.03.2016


s = 1000
## a very inefficient and stupid method! Brute force
##for i in range(2, 333):
##	for j in range(i+1, 996):
##		for k in range(j+1, 996):
##			if pow(i,2)+pow(j,2) == pow(k,2) and i+j+k == s:
##				print (i,j,k), (i*j*k)
##				break
##
# a faster method
for i in range(3, (s-3)/3):
    for j in range(i+1, (s-1-i)/2):
        k = s-i-j
        if pow(i,2)+pow(j,2) == pow(k,2):
            print (i,j,k), (i*j*k)
