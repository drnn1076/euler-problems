# -*- coding: utf-8 -*-
##Euler problem 10
##https://projecteuler.net/problem=10


## The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
## Find the sum of all the primes below two million.

## Ricardo Cedeño Montaña MSc.
## Berlin, Germany. 07.03.2016

def sumOfPrimes(limit):
    sumOfPrimes = 2
    for i in range(3, limit, 2):
        if i>3 and i%3==0:
            sumOfPrimes = sumOfPrimes
        elif i>5 and i%5==0:
            sumOfPrimes = sumOfPrimes
        else:
            if prime(i) == True:  
                sumOfPrimes += i
    return sumOfPrimes
        

from math import sqrt       
def prime(n):
    """assumes n is an int >= 2
       evaluates if n is a prime"""
    squareRoot1 = int(sqrt(n))  #Get the square root of the number
    pFactor = True #Control variable
    if n==2 or n==3 or n==5:
        pFactor = True
        return pFactor
    if n%2 == 0:
        pFactor = False
        return pFactor
    elif n%3 == 0:
        pFactor = False
        return pFactor
    elif n%5 == 0:
        pFactor = False
        return pFactor
    else:                     
        for i in range(7, squareRoot1+1, 2):# Use the square root as end of the loop
            if i%3 != 0 and i%5 !=0:
                #print i
                if n%i == 0: 
                    pFactor = False #if the number can be divided evenly is not a prime.
                    break 
    return pFactor
