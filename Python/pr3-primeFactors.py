# -*- coding: utf-8 -*-
##Euler problem 3
##https://projecteuler.net/problem=3

##The prime factors of 13195 are 5, 7, 13 and 29.
##What is the largest prime factor of the number 200 ?


##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 24.02.2016

from math import sqrt
def factors(n):
    """assumes n is an int >= 2
       returns a list of factors of n, below its square root"""
    squareRoot = int(sqrt(n)) # variable to store the square root of n
    factors = [] # list of factors
    if n == 0 or n == 1 or n == 2:
        return n
    else:
        for i in range(2, squareRoot+1): # uses the square root as the end of the loop
            if n%i == 0: #evaluates if i divides evenly n
                factors.append(i) # If so then add i to the list of factors
        return factors # returns the list of factors below the squareroot of n
        
def prime(n):
    """assumes n is an int >= 2
       evaluates if n is a prime"""
    squareRoot1 = int(sqrt(n))  #Get the square root of the number
    pFactor = True              #Control variable
    for i in range(2, squareRoot1+1):# Use the square root as end of the loop
        if n%i == 0:               
           pFactor = False #if the number can be divided evenly is not a prime.
    return pFactor 
    
def primeFactors(n):
    """assumes n is an int >= 2
       returns a list of prime factors of n below its square root"""
    squareRoot = int(sqrt(n)) # square root of n
    primeFactors = [] # stores the prime factors
    for i in range(2, squareRoot): # uses the square root as the upper limit for the loop
        if n%i == 0: # evaluates if i divides evenly n
            primeFactors.append(i) # if so then add to the list of primefactors
            squareRoot2 = int(sqrt(i)) # square root of i
            #print(primeFactors, i, squareRoot2)
            for j in range(2, squareRoot2+1):
                if i%j == 0:
                    #print(i)
                    primeFactors.remove(i)
                    break
    return  primeFactors, primeFactors[-1]