# -*- coding: utf-8 -*-
##Euler problem 7
##https://projecteuler.net/problem=7

## By listing the first six prime numbers: 
## 2, 3, 5, 7, 11, and 13, we can see that 
## the 6th prime is 13.
## What is the 10 001st prime number?

##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 06.03.2016

from math import sqrt       
def isPrime(n):
    """assumes n is an int >= 2
       evaluates whether n is a prime"""
    squareRoot1 = int(sqrt(n))  #Get the square root of the number
    pFactor = True #Control variable
    if n==2 or n==3 or n==5:
        pFactor = True
        return pFactor
    if n%2 == 0:
        pFactor = False
        return pFactor
    elif n%3 == 0:
        pFactor = False
        return pFactor
    elif n%5 == 0:
        pFactor = False
        return pFactor
    else:                     
        for i in range(7, squareRoot1+1, 2):# Use the square root as end of the loop
            if i%3 != 0 and i%5 !=0:
                if n%i == 0: 
                    pFactor = False #if the number can be divided evenly is not a prime.
                    break 
    return pFactor
    
target = 10000
n = 3 # since 2 is a prime, start from 3 and evaluates 10001 -1
while (target >= 1):
    if isPrime(n) == True:
        target -= 1
    n += 2
print (n - 2) # substract two of n to get the last number evaluated
