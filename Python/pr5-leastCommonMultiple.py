# -*- coding: utf-8 -*-
##Euler problem 5
##https://projecteuler.net/problem=4

## 2520 is the smallest number that can 
## be divided by each of the numbers from 
## 1 to 10 without any remainder.
## What is the smallest positive number 
## that is evenly divisible by all of the 
## numbers from 1 to 20?


##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 04.03.2016

#for x in range (2560, (math.factorial(20)/math.factorial(10)/math.factorial(5)/math.factorial(4)), 20):
#    if x%19 == 0:
#        if x%18 == 0:
#            if x%17 == 0:
#                if x%16 == 0:
#                    if x%15 == 0:
#                        if x%14 == 0:
#                            if x%13 == 0:
#                                if x%12 == 0:
#                                    if x%11 == 0:
#                                        print x
#                                        break                                       
## This checking is pointless since what we are loooking for is the 
## least commom multiple. 
## Coincidencially, the answer is the division of the factorials as proposed there.
## x2 = 2*1
## x3 = 3*1
## x4 = 2*2
## x5 = 5*1
## x6 = 2*3
## x7 = 7*1
## x8 = 2*2*2
## x9 = 3*3
## x10 = 2*5
## x11 = 11*1
## x12 = 2*3*2
## x13 = 13*1
## x14 = 2*7
## x15 = 3*5
## x16 = 2*2*2*2
## x17 = 17*1
## x18 = 2*3*3
## x19 = 19*1
## x20 = 2*2*5, thus
## thus lcm = x16(2^4)*x9(3^2)*x5*x7*x11*x13*x17*x19

def primeDivisors (dividend):
    divisor = 2## divisor = 2
    listOfDivisors = [] ## listOfDivisors = []
    if dividend < 2:
        return 1
    while divisor <= dividend:## while divisor <= n
        if dividend%divisor == 0:## if n%divisor == 0 then
            dividend = dividend/divisor    ## n = n/divisor
            listOfDivisors.append(divisor)## add divisor to listOfDivisors
        else:## else
            divisor += 1## increment divisor by 1
        if dividend == 0:## if n == 0
            break## break
    return listOfDivisors## return listOfDivisors
                                       
                                                                       
def lcm (n):
    """ asummes a number greater than 1
    and returns the least common multiple of all its
    preceding consecutive numbers inclusive."""
    metaListOfDivisors = []
    ListOfCommonMultipliers = []
    LeastCommonMultiple = 1
    for i in range (2, n+1): ## for each number until n
        # get the primeDivisors for i
        # add primeDivisors of i to metaList
        metaListOfDivisors.append(primeDivisors(i))
    for i in range(2, len(metaListOfDivisors)+2):
        if prime(i) == True:
        ## find the greater number of repetitions of each prime
            LeastCommonMultiple = LeastCommonMultiple*findGreaterPrimeRepetition(i, metaListOfDivisors)
        ## copy them to new finalList
            ListOfCommonMultipliers.append(LeastCommonMultiple )

    return ListOfCommonMultipliers, LeastCommonMultiple 

from math import sqrt       
def prime(n):
    """assumes n is an int >= 2
       evaluates if n is a prime"""
    squareRoot1 = int(sqrt(n))  #Get the square root of the number
    pFactor = True #Control variable
    if n%2 == 0:
        pFactor = False
        return pFactor
    elif n%3 == 0:
        pFactor = False
        return pFactor
    elif n%5 == 0:
        pFactor = False
        return pFactor
    else:
        for i in range(2, squareRoot1+1):# Use the square root as end of the loop
            #print i
            if n%i == 0:               
                pFactor = False #if the number can be divided evenly is not a prime.
    return pFactor       

        
def findGreaterPrimeRepetition(prime, listOfLists):
    product = 1
    if listOfLists[-1][-1] == prime:
        if listOfLists[-1][-1] == listOfLists[-1][0]:
            ## find the product of all members of listCommonMultiplers
            lista = listOfLists[-1]
            if len(lista)>1:
                for i in range(len(lista)): 
                    product = product*lista[i]
            else:
                product = lista[-1]
            print (lista, product)
            return product #listOfLists[-1]
    return findGreaterPrimeRepetition(prime, listOfLists[:-1])
