# -*- coding: utf-8 -*-
##Euler problem 2
##https://projecteuler.net/problem=2

##Each new term in the Fibonacci sequence is 
##generated by adding the previous two terms. 
##By starting with 1 and 2, the first 10 terms will be:
##1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
##By considering the terms in the Fibonacci sequence 
##whose values do not exceed four million, find the sum 
##of the even-valued terms.


##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 21.02.2016

limit = 4000000
fibonacci1 = 1
fibonacci2 = 1
fibonacci = 1
sumFibonacciEven = 0

while fibonacci<limit:
    fibonacci = (fibonacci1)+(fibonacci2)
    fibonacci1 = fibonacci
    fibonacci2 = fibonacci1
    if fibonacci%2==0:
        sumFibonacciEven = sumFibonacciEven + fibonacci
    print(sumFibonacciEven)