# -*- coding: utf-8 -*-
##Euler problem 4
##https://projecteuler.net/problem=4

## A palindromic number reads the same both ways. 
## The largest palindrome made from the product 
## of two 2-digit numbers is 9009 = 91 × 99.
## Find the largest palindrome made from the product 
## of two 3-digit numbers.

##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 03.03.2016

palindrome = 0
largestPalindrome = 0
for i in range(999, 100, -1): # two dimensional loop for numbers between 999 and 100
    for j in range(i, 100, -1): # j  starts from i each loop in order not to repeat multiplications
        n = i * j # get the product
        #print n, i, j
        if str(n) == str(n)[::-1]: # compare the product with its reverse 
            palindrome = n # assign it to palindrome variable
        if palindrome > largestPalindrome: # compare to the biggest palindrome found
            largestPalindrome = palindrome
print largestPalindrome # return the biggest palindrome found.
 