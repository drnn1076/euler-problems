# -*- coding: utf-8 -*-
##Euler problem 1
##https://projecteuler.net/problem=1

##If we list all the natural numbers below 10 
##that are multiples of 3 or 5, we get 3, 5, 6 and 9. 
##The sum of these multiples is 23.
##Find the sum of all the multiples of 3 or 5 
##below 1000.


##Ricardo Cedeño Montaña MSc.
##Berlin, Germany. 21.02.2016

def getNumbers(target):
    sumOfMultiples = 0
    for i in range(target):
        if i%3 == 0 or i%5 == 0:
            sumOfMultiples = sumOfMultiples+i
    return sumOfMultiples

##This function does the same but as the target
##gets higher it needs less time to do the calculation 
##faster. 
##It first uses the fact that the multiples of 3 up to 99 
##are equal to 3 * 33, where 99/3=33.
##Second, it uses the Gauss formula for adding consecutive numbers 
##beginning with 1: sum from 1 to n = (n(n+1))/2.
#e.g. sum of 1 to 10 = (10(11))/2 = 55.


def getNumbers1(target):
    return multiples(target, 3)+multiples(target,5)-multiples(target,15)

def multiples(target, number):       
    p=target/number
    sumOfMultiples= number*(p*(p+1))/2
    return sumOfMultiples        
             

                
    
